
var ms = require('../');



var net = new ms.Networking();

net.start(function () {

  var st = ms.ServiceType.wildcard;

  var packet = new ms.DNSPacket();
  packet.question.push(
    new ms.DNSRecord('_googlecast._tcp.local', ms.DNSRecord.Type.PTR, 1)
  );

  net.send(packet);

});

