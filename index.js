var dns = require('mdns-js-packet');

exports.Networking = require('./lib/networking');
exports.ServiceType = require('./lib/service_type').ServiceType;
exports.Server = require('./lib/server');

exports.DNSPacket = dns.DNSPacket;
exports.DNSRecord = dns.DNSRecord;

