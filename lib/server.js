
var debug = require('debug')('mdns:lib:server');

var util = require('util');
var EventEmitter = require('events').EventEmitter;

var Internal = require('./internal');


var Server = module.exports = function () {
  debug('new server instance');
  var internal = new Internal(this);

  this.stop = function () {
    return internal.stop();
  }

  this.connect = function (options, next) {
    return internal.connect(options, next);
  }

  this.createQuery = function (serviceType) {
    return internal.createQuery(serviceType);
  }
};

util.inherits(Server, EventEmitter);

// Server.prototype.addService = function (service) {
//   var probe = new Probe(service);
//   this.addProbe(probe);
//   return probe;
// };


