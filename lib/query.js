
var ServiceType = require('./service_type').ServiceType;
var util = require('util');
var EventEmitter = require('events').EventEmitter;

var Query = module.exports = function (serviceType) {
  var notString = typeof serviceType !== 'string';
  var notType = !(serviceType instanceof ServiceType);
  if (notString && notType) {
    debug('serviceType type:', typeof serviceType);
    debug('serviceType is ServiceType:', serviceType instanceof ServiceType);
    debug('serviceType=', serviceType);
    throw new Error('argument must be instance of ServiceType or valid string');
  }

  this.serviceType = serviceType;

}

util.inherits(Query, EventEmitter);
