var debug = require('debug')('mdns:lib:internal');
var util = require('util');
var EventEmitter = require('events').EventEmitter;

var fs = require('fs');
var Datastore = require('nedb');
var Networking = require('./networking');
var dns = require('mdns-js-packet');
var DNSRecord = dns.DNSRecord;
var ServiceType = require('./service_type').ServiceType;
var pf = require('./packetfactory');
var Probe = require('./probe');
var Query = require('./query');



function writeJson(filename, obj) {
  fs.writeFile(filename, JSON.stringify(obj, null, 4), function(err) {
    if(err) {
      debug(err);
    } else {
      debug("JSON saved to " + filename);
    }
  });
}



var Internal = module.exports = function (server) {
  this.server = server;
  this.uses = [];
  this.db = new Datastore();

  //Array  of active queries
  this.queries = [];

  // Array of pending probes.
  this.probes = [];
  // Array of published services.
  this.services = [];
}

Internal.prototype.dumpDb = function (callback) {
  this.db.find({}).sort({address: 1}).exec(callback);
}

Internal.prototype.stop = function () {
  debug('stopping');
  this.dumpDb(function (err, docs) {
    writeJson('db.json', docs);
  });
  this.networking.stop();
}


Internal.prototype.packetHandler = function (packets, remote) {
  var self = this;
  // self.emit('packets', packets, remote, connection);
  debug('got %s packet(s) from %s', packets.length, remote.address);
  if (packets.length > 0) {
    packets.forEach(function (packet) {

      debug('packet got %s questions, %s answers, %s authority, %s additional',
        packet.question.length,
        packet.answer.length,
        packet.authority.length,
        packet.additional.length);
      packet.question.forEach(handle(self.handleQuery));
      packet.answer.forEach(handle(self.handleAnswer));
    });
  }
  else {
    debug('!! error in parsing packages?', packets, remote);
  }

  function handle(fn) {
    return function (rec) {
      fn.bind(self)(rec, remote);
    }
  }
};


Internal.prototype.handleAnswer = function (rec, remote) {
  var self = this;
  // debug('handleAnswer "%s" type=%s, class=%s', rec.name, rec.typeName, rec.className);
  this.handleProbe(rec);
  switch (rec.type) {
    case DNSRecord.Type.PTR:
      self.answerPTR(rec, remote);
    break;
    case DNSRecord.Type.TXT:
      self.answerTXT(rec, remote);
      break;
    case DNSRecord.Type.A:
      self.answerA(rec, remote);
      break;
    default:
      debug('cant handle answer of type %s', rec.typeName)
      debug('rec', rec);
  }
}


Internal.prototype.handleQuery = function (rec) {
  debug('query "%s" type=%s, class=%s, %j', rec.name, rec.typeName, rec.className, rec);
  if (rec.type !== DNSRecord.Type.PTR &&
    rec.type !== DNSRecord.Type.SRV &&
    rec.type !== DNSRecord.Type.ANY) {
    debug('skipping query: type not PTR/SRV/ANY');
    return;
  }
  var self = this;

  // check if we should reply via multi or unicast
  // TODO: handle the is_qu === true case and reply directly to remote
  // var is_qu = (rec.cl & DNSRecord.Class.IS_QM) === DNSRecord.Class.IS_QM;
  rec.class &= ~DNSRecord.Class.IS_OM;
  if (rec.class !== DNSRecord.Class.IN && rec.type !== DNSRecord.Class.ANY) {
    debug('skipping query: class not IN/ANY: %d', rec.clas, rec);
    return;
  }
  try {
    var type = new ServiceType(rec.name);
    self.services.forEach(function (service) {
      if (type.isWildcard() || type.matches(service.serviceType)) {
        debug('answering query');
        // TODO: should we only send PTR records if the query was for PTR
        // records?
        self.send(
          pf.buildANPacket.apply(service, [DNSRecord.TTL]));
      } else {
        debug('skipping query; type %s not * or %s', type,
            service.serviceType);
      }
    });
  } catch (err) {
    debug('error in handleQuery', err);
    // invalid service type
  }
};


Internal.prototypeanswerA = function (rec, remote) {
  debug('answerA');
  var update = {$set: {fullname: rec.name}};
  this.upsertHostService(rec.address, update)
}


Internal.prototypeanswerTXT = function (rec, remote) {
  try {
    var s = new ServiceType(rec.name);
    var key = s.toString();
    var data = {};
    rec.data.forEach(function (row) {
      var kv = row.split('=');
      data[key + '.' + kv[0]] = kv[1];
    });
    data[key + '.hostname'] = s.descriptor;
    var update = {$set: data};
    debug('txt data', data);
    // internal.upsertHostname
    // internal.upsertHostService.bind(this)(remote.address, update);
    //debug('txt', s);
  }
  catch(err) {
    log.warn('error in answerTXT', err);
  }
}


Internal.prototype.answerPTR = function (rec, remote) {
  var self = this;
  try {
    if (rec.name.indexOf('_') === 0) {
      //probably a service of some kind
      var s = new ServiceType(rec.name.replace('.local', ''));
      var hostname;
      if (s.isWildcard()) {
        //data contains actual service
        if (rec.data === '') {
          //<Root>, just skip
          return;
          // throw new Error('record should have had .data');
        }
        s = new ServiceType(rec.data.replace('.local', ''));
      }
      // debug('service from %s', remote.address, s);
      self.upsertHostname(remote.address, s.descriptor);
      self.upsertHostService(remote.address, s.toString());
    }
    else {
      debug('strange PTR record in %s', sectionName, rec);
    }
  }
  catch(err) {
    debug('error in PTR', err, remote, rec);
  }
};


Internal.prototype.upsertHostname = function (address, hostname) {
  if (typeof hostname === 'undefined' || hostname === '') {
    return;
  }
  var update = {
    $set: {hostname: hostname}
  };
  try {
    var query = {address: address};
    debug('upsertHostname', query, update);
    this.db.update(query, update, {upsert: true}, function (err, numRep, doc) {
      //debug('upsertHostService', err, numRep, update);
      if(err) {
        debug('err.upsertHostname', err);
      }
    });
  }
  catch (err) {
    debug('error in upsertHostname', err);
  }
}


Internal.prototype.upsertHostService = function (address, service) {
  var update = {
    $addToSet: {services: service}
  };
  try {
    var query = {address: address};
    // debug('upsertHostService', query, update);
    this.db.update(query, update, {upsert: true}, function (err, numRep, doc) {
      //debug('upsertHostService', err, numRep, update);
      if(err) {
        debug('err.upsertHostService', err);
      }
    });
  }
  catch (err) {
    debug('error in upsertHostService', err);
  }
};


Internal.prototype.connect = function (options, next) {
  options = options || {};
  if (typeof next === 'undefined' && typeof options === 'function') {
    next = options;
    options = {};
  }
  debug('connect');
  this.networking = new Networking();
  this.networking.on('packets', this.packetHandler.bind(this));
  this.networking.start(next);
};


Internal.prototype.send = function (packet) {
  this.networking.send(packet)
}


Internal.prototype.addProbe = function (probe) {
  var self = this;
  this.probes.push(probe);
  setTimeout(function () {
    self.probeAndAdvertise(probe);
  }, 250);

};


Internal.prototype.removeProbe = function (probe) {
  debug('unpublishing service');
  this.services =
    this.services.filter(function (service) { return service !== probe; });

  this.send(pf.buildANPacket.apply(probe, [0]));
  probe.nameSuffix = '';
  probe.alias = '';
  probe.status = 0; // inactive

};


Internal.prototype.probeAndAdvertise = function (probe) {
  debug('probeAndAdvertise(%s)', probe.status);
  var self = this;
  switch (probe.status) {
    case 0:
    case 1:
    case 2:
      self.send(pf.buildQDPacket.apply(probe, []));
      break;
    case 3:
      debug('publishing service "%s", suffix=%s', probe.alias, probe.nameSuffix);
      var packet = pf.buildANPacket.apply(probe, [DNSRecord.TTL]);
      self.send(packet);
      // Repost announcement after 1sec (see rfc6762: 8.3)
      setTimeout(function onTimeout() {
        self.send(packet);
      }, 1000);

      // Service has been registered, respond to matching queries
      self.services.push(probe);

      // //remove probe from list once it's been registered
      self.probes =
        self.probes.filter(function (service) { return service === probe; });

      break;
    case 4:
      // we had a conflict
      if (probe.nameSuffix === '') {
        probe.nameSuffix = '1';
      } else {
        probe.nameSuffix = (parseInt(probe.nameSuffix) + 1) + '';
      }
      probe.status = -1;
      break;
  }

  if (probe.status < 3) {
    probe.status++;
    setTimeout(function () {
      self.probeAndAdvertise(probe);
    }, 250);
  }
};//--probeAndAdvertise



Internal.prototype.handleProbe = function (rec) {
  var self = this;
  try {
    self.probes.forEach(function (service) {
      if (service.status < 3) {
        var conflict = false;
        // parse answers and check if they match a probe
        debug('check names: %s and %s', rec.name, service.alias);
        switch (rec.type) {
          case DNSRecord.Type.PTR:
            if (rec.asName() === service.alias) {
              conflict = true;
              debug('name conflict in PTR');
            }
            break;
          case DNSRecord.Type.SRV:
          case DNSRecord.Type.TXT:
            if (rec.name === service.alias) {
              conflict = true;
              debug('name conflict in SRV/TXT');
            }
            break;
        }
        if (conflict) {
          // no more probes
          service.status = 4;
        }
      }
    });
  } catch (err) {
    // invalid service type
  }

};


Internal.prototype.createQuery = function (serviceType) {
  var self = this;
  if (typeof serviceType === 'undefined') {
    serviceType = ServiceType.wildcard;
  }
  var query = new Query(serviceType);
  this.queries.push(query);
  setTimeout(function () {
    debug('sending discovery packet');
    var packet = pf.buildServiceQuery.apply(query, []);
    self.send(packet);
  }, 100);
  return query;
}