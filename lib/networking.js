var debug = require('debug')('mdns:lib:networking');
var util = require('util');
var dgram = require('dgram');
var dns = require('mdns-js-packet');
var util = require('util');
var EventEmitter = require('events').EventEmitter;

var MDNS_MULTICAST = '224.0.0.251';




var Networking = module.exports = function (options) {
  this.opt = options || {};
  this.opt.interface = this.opt.interface || '0.0.0.0';
};


util.inherits(Networking, EventEmitter);

Networking.prototype.start = function (next) {
  var self = this;
  this.createSocket(this.opt.interface, 5353, onSocket)

  function onSocket(err, sock) {
    if (err) {return next(err); }
    self.sock = sock;
    return next(null);
  }
}

Networking.prototype.stop = function () {
  this.sock.close();
  this.sock.unref();
}


Networking.prototype.createSocket = function (address, port, next) {
  var self = this;
  var sock = dgram.createSocket('udp4');
  sock.bind(port, address, function (err) {
    sock.addMembership(MDNS_MULTICAST);
    sock.setMulticastTTL(255);
    sock.setMulticastLoopback(true);

    sock.on('message', function (message, remote) {
      debug('got a message');
      var packets = dns.DNSPacket.parse(message);
      if (!(packets instanceof Array)) {
        packets = [packets];
      }
      self.emit('packets', packets, remote);
      // console.log('packets', packets);
    });

    next(err, sock);
  });
};


Networking.prototype.send = function (packet) {
  var buf = dns.DNSPacket.toBuffer(packet);
  this.sock.send(buf, 0, buf.length, 5353, '224.0.0.251', function (err, bytes) {
    debug('sent %d bytes', bytes, err);
  })
};
